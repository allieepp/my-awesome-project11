import turtle

turtle.penup()
turtle.left(90)
turtle.pendown()
turtle.right(180)


tur = turtle.Turtle()
tur.goto(-150, 0)
tur.speed(20)
tur.color("Yellow", "black")


for i in range(50):
    tur.forward(300)
    tur.left(170)

tur.end_fill()

turtle.pensize(2)
turtle.fd(30)
turtle.left(90)
turtle.fd(25)
turtle.left(45)
turtle.fillcolor("green")
turtle.begin_fill()
turtle.circle(-80, 90)
turtle.right(90)
turtle.circle(-80, 90)
turtle.end_fill()
turtle.right(135)
turtle.fd(60)
turtle.left(180)
turtle.fd(85)
turtle.left(90)
turtle.fd(80)


turtle.right(90)
turtle.right(45)
turtle.fillcolor("green")
turtle.begin_fill()
turtle.circle(80, 90)
turtle.left(90)
turtle.circle(80, 90)
turtle.end_fill()
turtle.left(135)
turtle.fd(60)
turtle.left(180)
turtle.fd(60)
turtle.right(90)
turtle.circle(200, 60)
turtle.done()